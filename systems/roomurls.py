from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required
from django.contrib import admin
from systems import views

urlpatterns = patterns('',
                       url(r'^$', views.room_view, name='room_view'),
                       url(r'^detail/(?P<mypk>[0-9]+)/$',
                           views.room_detail, name='room_detail'),
                       url(r'^edit/(?P<mypk>-?\d+)/$',
                           views.room_edit, name='room_edit'),
                       )
