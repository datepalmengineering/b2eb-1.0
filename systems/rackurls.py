from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required
from django.contrib import admin
from systems import views

urlpatterns = patterns('',
                       url(r'^$', views.rack_view, name='rack_view'),
                       url(r'^detail/(?P<mypk>[0-9]+)/$',
                           views.rack_detail, name='rack_detail'),
                       url(r'^edit/(?P<mypk>-?\d+)/$',
                           views.rack_edit, name='rack_edit'),
                       )
