from rest_framework import generics
from django.forms.models import model_to_dict
from django.contrib.auth.models import User
from rest_framework.decorators import permission_classes
from rest_framework import permissions
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import renderers
from systems.models import Property


@api_view(['GET', 'POST'])
@permission_classes((permissions.AllowAny,))
def api_root(request):
    queryset = Property.objects.all()  # Required for DjangoModelPermissions
    if request.method == 'POST':
        action = None
        if "action" in request.data.keys():
            action = request.data["action"]

        if "pk" in request.data.keys():
            queryset = Property.objects.filter(pk=request.data["pk"])
            if queryset.exists():
                if action == "get":
                    return Response({"message": "Found a match!", "PK": queryset[
                                    0].pk, "key": queryset[0].key, "value": queryset[0].value})
                if action == "remove":
                    queryset[0].delete()
                    return Response(
                        {"message": "This would confirm PK removal."})
                if action == "edit_property":
                    queryset.update(
                        key=request.data["key"],
                        value=request.data["value"])
                    # queryset.save()
                    return Response({"message": "Property updated."})
        else:
            return Response({"message": "No match :("})
    return Response({"message": "Please post a valid request"})
