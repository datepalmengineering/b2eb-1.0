from rest_framework import generics
from django.forms.models import model_to_dict
from django.contrib.auth.models import User
from rest_framework.decorators import permission_classes
from rest_framework import permissions
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import renderers
from systems.models import System, Property, Room, Rack, Key
from servertools.views import replace_property
from django.contrib.auth.models import User


@api_view(['GET', 'POST'])
@permission_classes((permissions.AllowAny,))
def api_root(request):
    queryset = System.objects.all()  # Required for DjangoModelPermissions
    if request.method == 'POST':
        action = None
        if "action" in request.data.keys():
            action = request.data["action"]

        if "pk" in request.data.keys():
            queryset = System.objects.filter(pk=request.data["pk"])

        if queryset.exists():
            if action == "get":
                return Response({"message": "Found a match!",
                                 "PK": queryset[0].pk,
                                 "type": queryset[0].type,
                                 "hardware_type": queryset[0].hardware_type,
                                 "name": queryset[0].name,
                                 "servicetag": queryset[0].servicetag,
                                 "dell_id": queryset[0].dell_id,
                                 "asset_size": queryset[0].asset_size,
                                 "owner": queryset[0].owner.username,
                                 "asset_disposition": queryset[0].asset_disposition,
                                 "notes": queryset[0].notes,
                                 "room": str(queryset[0].room),
                                 "rack": str(queryset[0].rack),
                                 "properties": queryset[0].properties.values('pk',
                                                                             'value',
                                                                             'key')})
            if action == "remove":
                return Response({"message": "This would confirm PK removal."})
            if action == "add_property":
                mykey = Key.objects.filter(name=request.data["key"])
                if not mykey:
                    return Response({"message": "Invalid key provided."})

                newProperty = Property(
                    key=mykey[0], value=request.data["value"])
                newProperty.save()
                # This should create and add a property
                queryset[0].properties.add(newProperty)
                queryset[0].save()
                return Response({"message": "Property added."})
            if action == "replace_property":
                mykey = Key.objects.filter(name=request.data["key"])
                if not mykey:
                    return Response({"message": "Invalid key provided."})
                replace_property(
                    queryset[0],
                    mykey[0].name,
                    request.data["value"])
                return Response({"message": "Property replaced."})

        if action == "create":
            myServiceCode = request.data["servicetag"].strip()
            mySystem = System.objects.filter(servicetag=myServiceCode)

            if "room" in request.data.keys():
                myroom = Room.objects.none()
                myroomsearch = Room.objects.filter(name=request.data["room"])
                if not myroomsearch:
                    myroom = Room(name=request.data["room"],
                                  notes="Added by rest API",
                                  username="AUTO",
                                  entrytype="REST API UPDATE"
                                  )
                    myroom.save()
                else:
                    myroom = myroomsearch

            if "rack" in request.data.keys():
                myrack = Rack.objects.none()
                myracksearch = Rack.objects.filter(name=request.data["rack"])
                if not myracksearch:
                    myrack = Rack(name=request.data["rack"],
                                  notes="Added by rest API",
                                  username="AUTO",
                                  entrytype="REST API UPDATE"
                                  )
                    myrack.save()
                else:
                    myrack = myracksearch

            if mySystem.exists():
                myowner = mySystem[0].owner
            if "owner" in request.data.keys():
                myowner = User.objects.none()

                myownersearch = User.objects.filter(
                    username=request.data["owner"])
                if not myownersearch:
                    myowner = User(username=request.data["owner"])
                    myowner.save()
                else:
                    myowner = myownersearch[0]

            if mySystem.exists():
                mySystem.update(name=request.data["name"],
                                type=request.data["type"],
                                notes=request.data["notes"],
                                dell_id=request.data["dell_id"],
                                owned_hostnames=request.data[
                                    "owned_hostnames"],
                                asset_disposition=request.data[
                                    "asset_disposition"],
                                asset_size=request.data["asset_size"],
                                rack=myrack[0],
                                room=myroom[0],
                                owner=myowner,
                                username="AUTO",
                                entrytype="REST API CREATE")
                return Response(
                    {"Edited system with service code: " + myServiceCode})
            else:
                mySystem = System(servicetag=myServiceCode,
                                  name=request.data["name"],
                                  type=request.data["type"],
                                  notes=request.data["notes"],
                                  dell_id=request.data["dell_id"],
                                  owned_hostnames=request.data[
                                      "owned_hostnames"],
                                  asset_disposition=request.data[
                                      "asset_disposition"],
                                  asset_size=request.data["asset_size"],
                                  rack=myrack[0],
                                  room=myroom[0],
                                  owner=myowner,
                                  username="AUTO",
                                  entrytype="REST API UPDATE")
                mySystem.save()
                return Response(
                    {"Added system with service code: " + myServiceCode})
            return Response(
                {"message": "Create command received, but nothing actually happened... weird."})

    return Response({"message": "Please post a valid request"})
