from rest_framework import generics
from django.forms.models import model_to_dict
from django.contrib.auth.models import User
from rest_framework.decorators import permission_classes
from rest_framework import permissions
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import renderers
from systems.models import Cluster, System, Property


@api_view(['GET', 'POST'])
@permission_classes((permissions.AllowAny,))
def api_root(request):
    queryset = Cluster.objects.all()  # Required for DjangoModelPermissions
    if request.method == 'POST':
        action = None
        if "action" in request.data.keys():
            action = request.data["action"]
        queryset = Cluster.objects.filter(pk=request.data["pk"])

        if queryset.exists():
            if action == "get":
                return Response({"message": "Found a match!", "PK": queryset[0].pk, "notes": queryset[
                                0].notes, "properties": queryset[0].properties.values('pk', 'value', 'key')})
            if action == "remove":
                return Response({"message": "This would confirm PK removal."})
            if action == "add_property":
                newProperty = Property(
                    key=request.data["key"],
                    value=request.data["value"])
                newProperty.save()
                # This should create and add a property
                queryset[0].properties.add(newProperty)
                return Response({"message": "Property added."})
            if action == "attach_system":
                mySystem = System.objects.filter(pk=request.data["system_pk"])
                unavailableSystems = Cluster.objects.all().exclude(
                    pk=mypk).values_list('systems__pk', flat=True)
                if mySystem.pk not in unavailableSystems:
                    queryset[0].systems.add(newSystem)
                    queryset.save()
                return Response({"message": "System linked."})

        else:
            return Response({"message": "No match :("})
    return Response({"message": "Please post a valid request"})
