from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required
from django.contrib import admin
from systems import views
from systems import system_api_views, property_api_views
from rest_framework.urlpatterns import format_suffix_patterns


urlpatterns = patterns('',
                       url(r'^$', views.system_view, name='system_view'),
                       url(r'^api/$', system_api_views.api_root),
                       url(r'^property/api/$', property_api_views.api_root),
                       url(r'^type/(?P<systemType>.*)/$',
                           views.system_view_type, name='system_type'),
                       url(r'^parented/type/(?P<systemType>.*)/$',
                           views.system_view_type_with_parent, name='system_type'),
                       url(r'^detail/(?P<mypk>[0-9]+)/$',
                           views.system_detail, name='system_detail'),
                       url(r'^systemedit/(?P<mypk>-?\d+)/$',
                           views.system_edit, name='system_edit'),
                       url(r'^propertyedit/(?P<mypk>-?\d+)/$',
                           views.property_edit, name='property_edit'),
                       url(r'^propertyadd/(?P<mypk>-?\d+)/(?P<myowner>-?\d+)/$',
                           views.property_edit_link, name='property_add'),
                       url(r'^propertyremove/(?P<mypk>-?\d+)/(?P<myowner>-?\d+)/$',
                           views.property_edit_remove, name='property_remove'),
                       url(r'^systemdelete/(?P<mypk>-?\d+)/$',
                           views.system_delete, name='system_delete'),
                       url(r'^racadm_add/$',
                           views.racadm_system_add,
                           name='system_add'),
                       url(r'^assets.xml$', views.assets_to_xml,
                           name='assets_to_xml'),
                       url(r'^assets.csv$', views.assets_to_csv,
                           name='assets_to_csv'),
                       url(r'^racadm_add_single/$',
                           views.racadm_system_add_single,
                           name='system_add_single'),

                       )


# Login and logout views for the browsable API
urlpatterns += [url(r'^api-auth/$',
                    include('rest_framework.urls',
                            namespace='rest_framework')),
                ]
