from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render_to_response
from django.shortcuts import loader, render
from django.template import RequestContext
from django.http import HttpResponseRedirect
from systems.models import System, Property, Cluster
from django.forms import ModelForm, Textarea, SelectMultiple
from django.contrib.auth.decorators import user_passes_test
from django import forms
from django.contrib.auth.models import User
from servertools.views import update_host
import paramiko
# Create your views here.


class ClusterForm(ModelForm):
    required_css_class = 'required'

    def __init__(self, *args, **kwargs):
        super(ClusterForm, self).__init__(*args, **kwargs)
        # access object through self.instance...
        # if kwargs.get('instance'):
        mypk = kwargs['instance'].pk
        unavailableSystems = Cluster.objects.all().exclude(
            pk=mypk).values_list('systems__pk', flat=True)
        unavailableSystems = filter(None, unavailableSystems)
        self.fields['systems'].queryset = System.objects.filter(
            type__iexact="server").exclude(pk__in=unavailableSystems)

    class Meta:
        model = Cluster
        fields = ['name', 'owner', 'notes', 'systems']
        widgets = {
            'notes': Textarea(attrs={'cols': 30, 'rows': 10}),
            'system': SelectMultiple(),
        }


class ClusterFormSystemsOnly(ModelForm):
    required_css_class = 'required'

    def __init__(self, *args, **kwargs):
        super(ClusterFormSystemsOnly, self).__init__(*args, **kwargs)
        # access object through self.instance...
        # if kwargs.get('instance'):
        mypk = kwargs['instance'].pk
        unavailableSystems = Cluster.objects.all().exclude(
            pk=mypk).values_list('systems__pk', flat=True)
        unavailableSystems = filter(None, unavailableSystems)
        self.fields['systems'].queryset = System.objects.filter(
            type__iexact="server").exclude(pk__in=unavailableSystems)

    class Meta:
        model = Cluster
        fields = ['systems']
        widgets = {
            'system': SelectMultiple(),
        }


class ClusterFormOwnerChange(ModelForm):
    required_css_class = 'required'
    # owner = forms.ModelChoiceField(queryset=User.objects.order_by('username'))

    class Meta:
        model = Cluster
        fields = ['owner', 'notes']
        widgets = {
            'notes': Textarea(attrs={'cols': 30, 'rows': 10}),
        }


class SystemForm(ModelForm):
    required_css_class = 'required'

    class Meta:
        model = System
        fields = [
            'type',
            'hardware_type',
            'name',
            'servicetag',
            'dell_id',
            'asset_disposition',
            'asset_size',
            'owned_ips',
            'owned_macs',
            'owned_hostnames',
            'owner',
            'room',
            'rack',
            'notes']
        widgets = {
            'notes': Textarea(attrs={'cols': 30, 'rows': 10}),
        }


class PropertyForm(ModelForm):
    required_css_class = 'required'

    class Meta:
        model = Property
        fields = ['key', 'value', 'attachment']


def assets_to_xml(request):

    template = loader.get_template('systems/asset_tracking.xml')
    context = RequestContext(request, {
        # "myClusters": myClusters,
    })
    return HttpResponse(template.render(context))


def assets_to_csv(request):

    template = loader.get_template('systems/asset_tracking.csv')
    context = RequestContext(request, {
        # "myClusters": myClusters,
    })
    return HttpResponse(template.render(context))


def ownedby(request, username):
    if username == "None":
        myClusters = Cluster.objects.filter(
            owner__username=None).order_by('name')
    else:
        myClusters = Cluster.objects.filter(
            owner__username=username).order_by('name')

    template = loader.get_template('systems/clusters_with_menu.html')
    context = RequestContext(request, {
        "myClusters": myClusters,
    })
    return HttpResponse(template.render(context))


def myclusters_view(request):
    myClusters = {}
    if request.user.is_authenticated():
        if request.user.is_superuser:
            myClusters = Cluster.objects.all().order_by('name')
        else:
            myClusters = Cluster.objects.filter(
                owner=request.user).order_by('name')

    template = loader.get_template('systems/clusters_with_menu.html')
    context = RequestContext(request, {
        "myClusters": myClusters,
    })
    return HttpResponse(template.render(context))


@user_passes_test(lambda u: u.is_superuser)
def cluster_add_system(request, mypk):
    myCluster = {}
    if int(mypk) != -1:
        myCluster = Cluster.objects.filter(pk=mypk)[0]
    else:
        myCluster = Cluster(name="", notes="")

    message = ""

    form = ClusterFormSystemsOnly(request.POST or None, instance=myCluster)

    if request.method == 'POST':
        if form.is_valid():
            form.save()

            myCluster.save()

            for mySystem in myCluster.systems.all():
                mySystem.owner = myCluster.owner
                mySystem.save()
            template = loader.get_template('systems/redirect_template.html')
            context = RequestContext(request, {
                # /detail/"+str(myCluster.pk)+"/"
                'redirect_url': "/clusters/detail/" + str(myCluster.pk) + "/"
            })
            return HttpResponse(template.render(context))
        else:
            message = "invalid input, please adjust."

    template = loader.get_template('systems/clusteredit.html')
    context = RequestContext(request, {
        "mypk": mypk,
        "myCluster": myCluster,
        "message": message,
        "form": form.as_p,
    })
    return HttpResponse(template.render(context))


def cluster_detail(request, mypk):
    myCluster = Cluster.objects.get(pk=mypk)

    template = loader.get_template('systems/clusterdetail.html')
    context = RequestContext(request, {
        "myCluster": myCluster,
        "mypk": mypk,
    })
    return HttpResponse(template.render(context))


@user_passes_test(lambda u: u.is_superuser)
def cluster_remove_system(request, mypk, myowner):
    mySystem = System.objects.filter(pk=mypk)[0]
    mySystem.owner = None
    mySystem.save()

    myCluster = Cluster.objects.filter(pk=myowner)[0]
    myCluster.systems.remove(mySystem)
    myCluster.save()

    template = loader.get_template('systems/redirect_template.html')
    context = RequestContext(request, {
        'redirect_url': "/clusters/detail/" + myowner + "/"
    })
    return HttpResponse(template.render(context))


@user_passes_test(lambda u: u.is_superuser)
def cluster_property_remove(request, mypk, myowner):
    myProperty = Property.objects.filter(pk=mypk)[0]
    myProperty.delete()

    template = loader.get_template('systems/redirect_template.html')
    context = RequestContext(request, {
        'redirect_url': "/clusters/detail/" + myowner + "/"
    })
    return HttpResponse(template.render(context))


def cluster_view(request):
    myClusters = Cluster.objects.all()

    template = loader.get_template('systems/clusters_with_menu.html')
    context = RequestContext(request, {
        "myClusters": myClusters,
    })
    return HttpResponse(template.render(context))


@user_passes_test(lambda u: u.is_superuser)
def cluster_delete(request, mypk):
    myCluster = Cluster.objects.filter(pk=mypk)[0]
    myCluster.properties.all().delete()
    for mySystem in myCluster.systems.all():
        mySystem.owner = None
        mySystem.save()
    myCluster.systems.clear()
    myCluster.delete()
    template = loader.get_template('systems/redirect_template.html')
    context = RequestContext(request, {
        'redirect_url': "/clusters/"
    })
    return HttpResponse(template.render(context))


def cluster_change_owner(request, mypk):
    myCluster = Cluster.objects.filter(pk=mypk)[0]
    myCluster.notes = ""
    form = ClusterFormOwnerChange(request.POST or None, instance=myCluster)
    message = ""
    if request.method == 'POST':
        if form.is_valid():
            form.save()

            myCluster.save()
            for mySystem in myCluster.systems.all():
                if myCluster.owner:
                    mySystem.owner = myCluster.owner
                else:
                    mySystem.owner = None

                mySystem.save()
            template = loader.get_template('systems/redirect_template.html')
            context = RequestContext(request, {
                'redirect_url': "/clusters/detail/" + str(myCluster.pk) + "/"
            })
            return HttpResponse(template.render(context))
        else:
            message = "invalid input, please adjust."

    template = loader.get_template('systems/clusteredit.html')
    context = RequestContext(request, {
        "mypk": mypk,
        "myCluster": myCluster,
        "message": message,
        "form": form.as_p,
    })
    return HttpResponse(template.render(context))


@user_passes_test(lambda u: u.is_superuser)
def cluster_edit(request, mypk):
    myCluster = {}
    if int(mypk) != -1:
        myCluster = Cluster.objects.filter(pk=mypk)[0]
    else:
        myCluster = Cluster(name="", notes="")

    message = ""

    form = ClusterForm(request.POST or None, instance=myCluster)

    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        # now created before und stuff
        # check whether it's valid:
        if form.is_valid():
            form.save()

            myCluster.save()

            for mySystem in myCluster.systems.all():
                mySystem.owner = myCluster.owner
                mySystem.save()

            template = loader.get_template('systems/redirect_template.html')
            context = RequestContext(request, {
                # /detail/"+str(myCluster.pk)+"/"
                'redirect_url': "/clusters/detail/" + str(myCluster.pk) + "/"
            })
            return HttpResponse(template.render(context))
        else:
            message = "invalid input, please adjust."

    template = loader.get_template('systems/clusteredit.html')
    context = RequestContext(request, {
        "mypk": mypk,
        "myCluster": myCluster,
        "message": message,
        "form": form.as_p,
    })
    return HttpResponse(template.render(context))


@user_passes_test(lambda u: u.is_superuser)
def property_edit_cluster(request, mypk, myowner):
    myProperty = {}
    if int(mypk) != -1:
        myProperty = Property.objects.filter(pk=mypk)[0]
    else:
        myProperty = Property()

    message = ""

    form = PropertyForm(
        request.POST or None,
        request.FILES or None,
        instance=myProperty)

    if request.method == 'POST':
        if form.is_valid():
            form.save(commit=True)

            if int(myowner) != -1:
                myCluster = Cluster.objects.filter(pk=myowner)[0]
                myCluster.properties.add(myProperty)
                myCluster.save()

            template = loader.get_template('systems/redirect_template.html')
            context = RequestContext(request, {
                'redirect_url': "/clusters/detail/" + myowner + "/"
            })
            return HttpResponse(template.render(context))
        else:
            message = "invalid input, please adjust."

    template = loader.get_template('systems/propertyedit.html')
    context = RequestContext(request, {
        "mypk": mypk,
        "myowner": myowner,
        "myProperty": myProperty,
        "message": message,
        "form": form.as_p,
    })
    return HttpResponse(template.render(context))


def system_view(request):
    mySystems = System.objects.all().order_by('name')

    template = loader.get_template('systems/systems.html')
    context = RequestContext(request, {
        "mySystems": mySystems,
    })
    return HttpResponse(template.render(context))


def system_view_type(request, systemType):
    mySystems = System.objects.all().filter(type=systemType).order_by('name')

    template = loader.get_template('systems/systems.html')
    context = RequestContext(request, {
        "mySystems": mySystems,
    })
    return HttpResponse(template.render(context))


def system_view_type_with_parent(request, systemType):
    mySystems = System.objects.all().filter(type=systemType).order_by('name')

    for system in mySystems:
        system.parent = "-"
        system.parentname = "-"
        system.ru = ""
        for prop in system.properties.all():
            if prop.key.name == "PARENT_ICN":
                system.parent = prop.value
                try:
                    system.parentname = System.objects.all().filter(
                        pk=int(prop.value) - 100000)[0].name
                except:
                    system.parentname = "ERROR NOT FOUND"
            if prop.key.name == "RU":
                system.ru = prop.value

    template = loader.get_template('systems/systems_with_parents.html')
    context = RequestContext(request, {
        "mySystems": mySystems,
    })
    return HttpResponse(template.render(context))


def system_detail(request, mypk):
    mySystem = System.objects.get(pk=mypk)

    template = loader.get_template('systems/detail.html')
    context = RequestContext(request, {
        "mySystem": mySystem,
        "mypk": mypk,
    })
    return HttpResponse(template.render(context))


@user_passes_test(lambda u: u.is_superuser)
def system_edit(request, mypk):
    mySystem = {}
    if int(mypk) != -1:
        mySystem = System.objects.filter(pk=mypk)[0]
    else:
        mySystem = System(type="", name="")

    message = ""

    form = SystemForm(request.POST or None, instance=mySystem)

    if request.method == 'POST':
        if form.is_valid():
            if int(mypk) == -1:
                mySystem = System(
                    type=form.cleaned_data['type'],
                    hardware_type=form.cleaned_data['hardware_type'],
                    name=form.cleaned_data['name'],
                    owner=form.cleaned_data['owner'],
                    notes=form.cleaned_data['notes'],
                    servicetag=form.cleaned_data['servicetag'],
                    dell_id=form.cleaned_data['dell_id'],
                    asset_disposition=form.cleaned_data['asset_disposition'],
                    asset_size=form.cleaned_data['asset_size'],
                    owned_ips=form.cleaned_data['owned_ips'],
                    owned_macs=form.cleaned_data['owned_macs'],
                    owned_hostnames=form.cleaned_data['owned_hostnames'],
                    room=form.cleaned_data['room'],
                    rack=form.cleaned_data['rack'],
                    username=str(request.user),
                    entrytype="Edit"
                )
            else:
                mySystem = System.objects.filter(pk=mypk)
                mySystem.update(
                    type=form.cleaned_data['type'],
                    hardware_type=form.cleaned_data['hardware_type'],
                    name=form.cleaned_data['name'],
                    owner=form.cleaned_data['owner'],
                    notes=form.cleaned_data['notes'],
                    servicetag=form.cleaned_data['servicetag'],
                    dell_id=form.cleaned_data['dell_id'],
                    asset_disposition=form.cleaned_data['asset_disposition'],
                    asset_size=form.cleaned_data['asset_size'],
                    owned_ips=form.cleaned_data['owned_ips'],
                    owned_macs=form.cleaned_data['owned_macs'],
                    owned_hostnames=form.cleaned_data['owned_hostnames'],
                    room=form.cleaned_data['room'],
                    rack=form.cleaned_data['rack'],
                    username=str(request.user),
                    entrytype="Edit"
                )
                mySystem = mySystem[0]

            mySystem.save()
            template = loader.get_template('systems/redirect_template.html')
            context = RequestContext(request, {
                'redirect_url': "/systems/detail/" + str(mySystem.pk) + "/"
            })
            return HttpResponse(template.render(context))
        else:
            message = "invalid input, please adjust."

    template = loader.get_template('systems/systemedit.html')
    context = RequestContext(request, {
        "mypk": mypk,
        "mySystem": mySystem,
        "message": message,
        "form": form.as_p,
    })
    return HttpResponse(template.render(context))


@user_passes_test(lambda u: u.is_superuser)
def property_edit(request, mypk):
    return property_edit_link(request, mypk, -1)


@user_passes_test(lambda u: u.is_superuser)
def property_edit_link(request, mypk, myowner):
    myProperty = {}
    if int(mypk) != -1:
        myProperty = Property.objects.filter(pk=mypk)[0]
    else:
        myProperty = Property()

    message = ""

    form = PropertyForm(
        request.POST or None,
        request.FILES or None,
        instance=myProperty)

    if request.method == 'POST':
        if form.is_valid():
            form.save(commit=True)

            if int(myowner) != -1:
                mySystem = System.objects.filter(pk=myowner)[0]
                mySystem.properties.add(myProperty)
                mySystem.save()

            template = loader.get_template('systems/redirect_template.html')
            context = RequestContext(request, {
                'redirect_url': "/systems/detail/" + myowner + "/"
            })
            return HttpResponse(template.render(context))
        else:
            message = "invalid input, please adjust."

    template = loader.get_template('systems/propertyedit.html')
    context = RequestContext(request, {
        "mypk": mypk,
        "myowner": myowner,
        "myProperty": myProperty,
        "message": message,
        "form": form.as_p,
    })
    return HttpResponse(template.render(context))


@user_passes_test(lambda u: u.is_superuser)
def property_edit_remove(request, mypk, myowner):
    myProperty = Property.objects.filter(pk=mypk)[0]
    myProperty.delete()

    template = loader.get_template('systems/redirect_template.html')
    context = RequestContext(request, {
        'redirect_url': "/systems/detail/" + myowner + "/"
    })
    return HttpResponse(template.render(context))


@user_passes_test(lambda u: u.is_superuser)
def system_delete(request, mypk):
    mySystem = System.objects.filter(pk=mypk)[0]
    mySystem.properties.all().delete()
    # mySystem.save()
    mySystem.delete()
    template = loader.get_template('systems/redirect_template.html')
    context = RequestContext(request, {
        'redirect_url': "/systems/"
    })
    return HttpResponse(template.render(context))


class racadm_form(
        forms.Form):  # Note that it is not inheriting from forms.ModelForm
    required_css_class = 'required'
    hostlist = forms.CharField(
        widget=forms.Textarea(
            attrs={
                'cols': 30,
                'rows': 10}),
    )


def racadm_system_add(request):
    response = ""
    form = racadm_form()
    if request.method == 'POST':
        form = racadm_form(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            hostlist = cd.get('hostlist').strip().split(',')
            for myhost in hostlist:
                response += update_host(myhost) + "<br>\n"

            return HttpResponse(response)

    template = loader.get_template('systems/racadm_add.html')
    context = RequestContext(request, {
        'form': form
    })
    return HttpResponse(template.render(context))


def racadm_system_add_single(request):
    response = ""
    if "host" in request.GET:
        host = request.GET["host"]
        if host:
            response = update_host(host)
            return HttpResponse(
                "<html><header><script>function f() { parent.location.reload(); } window.setTimeout(f, 5);</script></header><body>" +
                response +
                "</body></html>")
    return HttpResponse("Error: No host provided.")
