from django.db import models
from simple_history.models import HistoricalRecords
from django.contrib.auth.models import User
from systems.storage import OverwriteStorage
import os
# Create your models here.


def content_filename_name(instance, filename):
    return '/'.join([unicode(instance.key), unicode(instance.value), filename])


class Key(models.Model):
    name = models.CharField(max_length=64, unique=True)
    notes = models.CharField(max_length=512, null=True, blank=True)

    def __str__(self):              # __unicode__ on Python 2
        return str(self.name)

    class Meta:
        ordering = ['name']


class Property(models.Model):
    history = HistoricalRecords()
    key = models.ForeignKey(Key)
    value = models.CharField(max_length=512)
    attachment = models.FileField(
        upload_to=content_filename_name,
        storage=OverwriteStorage(),
        blank=True,
        null=True,
        default=None)
    username = models.CharField(max_length=128, default="Automatic")
    entrytype = models.CharField(max_length=128, default="Automatic")

    def __str__(self):              # __unicode__ on Python 2
        return str(self.key.name) + " => " + str(self.value)

    def delete(self):
        try:
            if os.path.isfile(self.attachment.path):
                os.remove(self.attachment.path)
        except:
            print("File detection error occured. Non-existent file.")
        super(Property, self).delete()


class Room(models.Model):
    history = HistoricalRecords()
    name = models.CharField(max_length=128)
    owner = models.ForeignKey(User, null=True, blank=True)
    notes = models.CharField(max_length=512)
    properties = models.ManyToManyField(Property, null=True, blank=True)
    username = models.CharField(max_length=128, default="Automatic")
    entrytype = models.CharField(max_length=128, default="Automatic")

    def __str__(self):              # __unicode__ on Python 2
        return str(self.name) + " ID =>" + str(self.pk) + \
            ", Name =>" + str(self.name)


class Rack(models.Model):
    history = HistoricalRecords()
    name = models.CharField(max_length=128)
    owner = models.ForeignKey(User, null=True, blank=True)
    room = models.ForeignKey(Room, null=True, blank=True)
    notes = models.CharField(max_length=512)
    properties = models.ManyToManyField(Property, null=True, blank=True)
    username = models.CharField(max_length=128, default="Automatic")
    entrytype = models.CharField(max_length=128, default="Automatic")

    def __str__(self):              # __unicode__ on Python 2
        return str(self.name) + " ID =>" + str(self.pk) + \
            ", Name =>" + str(self.name)


class HardwareTypes(models.Model):
    name = models.CharField(max_length=128)
    description = models.CharField(max_length=512)

    def __str__(self):              # __unicode__ on Python 2
        return str(self.name) + " " + str(self.description)


class Category(models.Model):
    name = models.CharField(max_length=128)
    description = models.CharField(max_length=512)

    def __str__(self):              # __unicode__ on Python 2
        return str(self.name) + " " + str(self.description)


class Disposition(models.Model):
    name = models.CharField(max_length=128)
    description = models.CharField(max_length=512)

    def __str__(self):              # __unicode__ on Python 2
        return str(self.name) + " " + str(self.description)


class Asset(models.Model):  # System/Asset
    history = HistoricalRecords()
    name = models.CharField(max_length=128)
    category = models.ForeignKey(Category, null=True, blank=True)
    owner = models.ForeignKey(User, null=True, blank=True)
    room = models.ForeignKey(Room, null=True, blank=True)
    rack = models.ForeignKey(Rack, null=True, blank=True)
    disposition = models.ForeignKey(Disposition, null=True, blank=True)
    notes = models.CharField(max_length=512, null=True, blank=True)
    properties = models.ManyToManyField(Property, blank=True)
    username = models.CharField(max_length=128, default="Automatic")
    entrytype = models.CharField(max_length=128, default="Automatic")

    def __str__(self):              # __unicode__ on Python 2
        return str(self.name) + " " + str(self.category) + \
            " ID =>" + str(self.pk)


class System(models.Model):  # System/Asset
    history = HistoricalRecords()
    TYPE_CHOICES = (
        ("server", "Physical Server"),
        ("vm", "Virtual Machine"),
        ("storage", "Permanent Storage Device"),
        ("HDD", "Hard Drives"),
        ("memory", "Memory"),
        ("switch", "Switch"),
        ("router", "Router"),
        ("power_management", "Power Management"),
        ("box", "Box with stuff in it")
    )
    type = models.CharField(max_length=128, choices=TYPE_CHOICES)
    hardware_type = models.ForeignKey(HardwareTypes, null=True, blank=True)
    name = models.CharField(max_length=128)
    servicetag = models.CharField(max_length=64, unique=True)
    dell_id = models.IntegerField(max_length=64, null=True, blank=True)

    SIZE_CHOICES = (
        ("1U", "1U"),
        ("2U", "2U"),
        ("3U", "3U"),
        ("4U", "4U"),
        ("5U", "5U"),
        ("6U", "6U"),
        ("7U", "7U"),
        ("8U", "8U"),
        ("9U", "9U"),
        ("10U", "10U")
    )
    asset_size = models.CharField(
        max_length=64,
        choices=SIZE_CHOICES,
        null=True,
        blank=True)
    owned_ips = models.CharField(max_length=256, null=True, blank=True)
    owned_macs = models.CharField(max_length=256, null=True, blank=True)
    owned_hostnames = models.CharField(max_length=512, null=True, blank=True)

    owner = models.ForeignKey(User, null=True, blank=True)
    room = models.ForeignKey(Room, null=True, blank=True)
    rack = models.ForeignKey(Rack, null=True, blank=True)

    DISPOSITION_CHOICES = (
        (0, 'Out of service, in storage'),
        (5, 'Out of service, newly-received or deployment pending'),
        (6, 'Out of service, in or awaiting repair'),
        (10, 'In service'),
        (80, 'In service, flagged for disposal'),
        (90, 'Out of service, awaiting disposal'),
        (99, 'Out of service, disposed of')
    )
    asset_disposition = models.IntegerField(
        'Disposition', choices=DISPOSITION_CHOICES, default=0)

    notes = models.CharField(max_length=512, null=True, blank=True)
    properties = models.ManyToManyField(Property, null=True, blank=True)
    username = models.CharField(max_length=128, default="Automatic")
    entrytype = models.CharField(max_length=128, default="Automatic")

    def __str__(self):              # __unicode__ on Python 2
        return str(self.name) + " " + str(self.type) + \
            " Service Tag =>" + str(self.servicetag)


class Cluster(models.Model):
    history = HistoricalRecords()
    name = models.CharField(max_length=128)

    owner = models.ForeignKey(User, null=True, blank=True)
    notes = models.CharField(max_length=512)
    properties = models.ManyToManyField(Property, null=True, blank=True)
    systems = models.ManyToManyField(System, null=True, blank=True)
    username = models.CharField(max_length=128, default="Automatic")
    entrytype = models.CharField(max_length=128, default="Automatic")

    def __str__(self):              # __unicode__ on Python 2
        return str(self.name) + " ID =>" + str(self.pk) + \
            ", Name =>" + str(self.name)
