from django.contrib import admin
from systems.models import Room, Rack, Property, System, Category, Disposition
from systems.models import Cluster, Key, HardwareTypes, Asset
# Register your models here.
admin.site.register(Room)
admin.site.register(Rack)
admin.site.register(Key)
admin.site.register(Property)
admin.site.register(System)
admin.site.register(Cluster)
admin.site.register(HardwareTypes)
admin.site.register(Asset)
admin.site.register(Category)
admin.site.register(Disposition)
