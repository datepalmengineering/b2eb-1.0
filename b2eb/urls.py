from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from rest_framework.urlpatterns import format_suffix_patterns
from django.contrib.auth.decorators import login_required
admin.autodiscover()
urlpatterns = patterns('',
                       # Examples:
                       url(r'^$', 'systems.views.myclusters_view', name='home'),
                       url(r'^systems/', include('systems.urls')),
                       url(r'^clusters/', include('systems.clusterurls')),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^accounts/',
                           include('accounts.urls',
                                   namespace='accounts')),
                       # url(r'^servertools/', include('servertools.urls',
                       # namespace='servertools')), urls.py missing
                       )
urlpatterns += format_suffix_patterns(urlpatterns)
if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
