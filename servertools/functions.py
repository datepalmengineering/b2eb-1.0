import requests  # need to install with kickstart
import platform
import psutil  # need to install with kickstart
import socket
name = platform.uname()[1].rstrip('.yourdomain.com')
fqdn = socket.getfqdn()
cores = psutil.cpu_count()
memory = psutil.virtual_memory().total >> 20
processor = platform.machine()
archetecture = platform.processor()
kernel = platform.release()
oscheck = platform.system()
if oscheck == "Linux":
    os = platform.dist()[0]
else:
    os = platform.win32_ver()[0]

serverclass = [
    "ATL",
    "ADD",
    "BIL",
    "BOS",
    "CHT",
    "COD",
    "DEN",
    "DET",
    "FAI",
    "GLA",
    "HYD",
    "HKG",
    "HNL",
    "JFK",
    "LAS",
    "MAD",
    "MIA",
    "PVD",
    "ROW",
    "SFO",
    "SLC",
    "SEA",
    "SUX",
    "VDZ"]
count = 0
sfound = 0
payload = {
    'name': name,
    'fqdn': fqdn,
    'cores': cores,
    'memory': memory,
    'processor': processor,
    'archetecture': archetecture,
    'kernel': kernel,
    'os': os}

for scheck in serverclass:
    if scheck.lower() in name.lower():
        r = requests.get('http://b2eb.yourdomain.com/servers/')
        slist = r.json()
        for s in slist:
            if s['name'] == name:
                for keys in payload:
                    sfound = 1
                    if payload[keys] != s[keys]:
                        server = slist[count]['id']
                        address = "http://b2eb.yourdomain.com/servers/%s/" % server
                        r = requests.put(
                            address,
                            auth=(
                                'root',
                                'cleverpassword'),
                            data=payload)
                        break
            else:
                count += 1
        if sfound == 0:
            r = requests.post(
                'http://b2eb.yourdomain.com/servers/',
                auth=(
                    'root',
                    'cleverpassword'),
                data=payload)
