﻿from django.shortcuts import render

from django.http import HttpResponse
import socket
import os
import subprocess
from systems.models import System, Property, Cluster, Key
import paramiko

SITE_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))


def check_ping(hostname):
    response = os.system("ping -c 1 -w 1 " + hostname)
    return response


def check_port(hostname, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(1)
    try:
        s.connect((hostname, port))
        print "Port 22 reachable"
        return True
    except socket.error as e:
        print "Error on connect: %s" % e
        return False
    s.close()


def update_host_view(request, hostname):
    return HttpResponse(str(update_host(hostname)))


def add_property(mySystem, key, value):
    # given a system object this should create a new property and link it to
    # the provided system
    myKey = Key.objects.get(name=key)
    myProperty = Property(key=myKey, value=value)
    myProperty.save()
    mySystem.properties.add(myProperty)
    mySystem.save()
    return True


def remove_property_key_exact(mySystem, key):
    myProperties = mySystem.properties.filter(key__name=key)
    myProperties.delete()
    return True


def remove_property_key_search(mySystem, term):
    # this should remove any properties from mySystem
    # whose key contains term
    return True


def replace_property(mySystem, key, value):
    # this should remove properties with key from mySystem,
    # and create a new property key:value in its place
    # removes all instances of a key for a system so be careful
    remove_property_key_exact(mySystem=mySystem, key=key)
    # will create duplicates if removal is not performed first
    add_property(mySystem=mySystem, key=key, value=value)
    return

winexecredentials = "/root%cleverpassword"


def winexe_call(hostname, domain, command):
    cmd = "/usr/bin/winexe -U " + \
        str(domain) + winexecredentials + " //" + \
        hostname + " \"powershell " + command + " \""
    process = subprocess.Popen(
        cmd,
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        close_fds=True)
    process.wait()
    stdout, stderr = process.communicate()
    if process.returncode != 0:
        return str(stderr)
    else:
        return str(stdout)


def update_create_drive(name, wwn, size):
    mySystem = System.objects.none()
    mySystem = System.objects.filter(servicetag=wwn)
    if mySystem.exists():
        mySystem.update(type="drive",
                        name=name,
                        notes="Updated drive through remote protocols.",
                        username="AUTO",
                        entrytype="Remote Update")
        mySystem = mySystem[0]
    else:
        mySystem = System(servicetag=wwn,
                          type="drive",
                          name=name,
                          notes="Created server through remote protocols.",
                          username="AUTO",
                          entrytype="Remote Update")
        mySystem.save()

    replace_property(mySystem, "SIZE_BYTES", str(size))


def racadm_add(myhost):
    hostname = myhost.strip() + "c.yourdomain.com"
    password = "cleverpassword"
    command = "racadm getsvctag"
    debug = ""
    username = "root"
    port = 22

    myServiceCode = ""

    try:
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        client.connect(
            hostname,
            port=port,
            username=username,
            password=password)

        stdin, stdout, stderr = client.exec_command(command)
        myServiceCode = stdout.read().replace("\n", " ").strip()
        debug = "Original " + myServiceCode

    finally:
        client.close()

    mySystem = System.objects.none()

    if myServiceCode != "":
        # deal with old outdated IDRAC versions
        if " " in myServiceCode:
            myServiceCode = myServiceCode.split(' ')
            debug = debug + " => Split " + str(myServiceCode)
            myServiceCode = myServiceCode[-1]
            debug = debug + " => Last Element " + str(myServiceCode)
        myServiceCode = myServiceCode.strip()
        mySystem = System.objects.filter(servicetag=myServiceCode)
        if mySystem.exists():
            mySystem.update(
                name=myhost + " server",
                type="server",
                notes="Updated server through IDRAC RACADM protocols.",
                owned_hostnames=myhost,
                username="AUTO",
                entrytype="RACADM")
            return ["Edited system " + myhost + " with service code: " +
                    myServiceCode + "<br> Debug [" + debug + "]", mySystem]
        else:
            mySystem = System(
                servicetag=myServiceCode,
                type="server",
                name=myhost + " server",
                notes="Created server through IDRAC RACADM protocols.",
                owned_hostnames=myhost,
                username="AUTO",
                entrytype="RACADM")
            mySystem.save()
            return ["Added system " + myhost + " with service code: " +
                    myServiceCode + "<br> Debug [" + debug + "]", mySystem]
    else:
        return [
            "Unable to reach " +
            myhost +
            " via ssh, if this system is not racadm " +
            "compatible this process will not work." +
            "<br> Debug [" +
            debug +
            "]",
            mySystem]


def update_host(hostname):

    # This pulls the system object from the actual racadm process
    mySystem = racadm_add(hostname)[1]

    fqh = hostname + ".yourdomain.com"
    if mySystem.exists():
        if check_ping(fqh) == 0:

            if check_port(fqh, 135):
                # --Clear and register hostname
                replace_property(
                    mySystem=mySystem[0],
                    key="HOST",
                    value=hostname)

                # --store ip
                myIP = str(socket.gethostbyname(fqh))
                replace_property(mySystem=mySystem[0], key="IP", value=myIP)
                mySystem.update(owned_ips=str(myIP))
                # --determine and store network
                myDomain = ""
                if myIP.startswith('172.'):
                    myDomain = "atc2"
                elif myIP.startswith('10.'):
                    myDomain = "www"
                replace_property(
                    mySystem=mySystem[0],
                    key="NETWORK",
                    value=myDomain)

                # --Clear and register operating system family as Windows
                replace_property(
                    mySystem=mySystem[0],
                    key="OS_FAMILY",
                    value="WINDOWS")

                # -open attempt windows remote procedure callable

                # --Clear and register bios version
                # Command: systeminfo | findstr /I /c:bios
                myCommand = "systeminfo | findstr /I /c:bios"
                myResponse = winexe_call(fqh, myDomain, myCommand)
                replace_property(
                    mySystem=mySystem[0],
                    key="BIOS",
                    value=myResponse)
                # remove "BIOS version:" and strip()
                # --Clear and register Processor Architecture
                # $ENV:Processor_Architecture
                myCommand = "reg query 'HKLM\System\CurrentControlSet\Control\Session" \
                            " Manager\Environment' /v PROCESSOR_ARCHITECTURE"
                myResponse = winexe_call(fqh, myDomain, myCommand)
                replace_property(mySystem=mySystem[
                                 0], key="CPU_ARCHITECTURE",
                                 value=myResponse.split("PROCESSOR_ARCHITECTURE")[-1])
                # --Clear and register number of CPUs
                # (Get-WmiObject -class Win32_Processor).count
                myCommand = "(Get-WmiObject -class Win32_Processor).count"
                myResponse = winexe_call(fqh, myDomain, myCommand)
                replace_property(
                    mySystem=mySystem[0],
                    key="CPU_SOCKETS",
                    value=myResponse)
                # --Clear and register number of cores
                # Invoke-Expression (((Get-WmiObject –class
                # Win32_processor).NumberOfCores) -join "+")
                myCommand = "Invoke-Expression (((Get-WmiObject -class Win32_processor)" \
                    ".NumberOfCores) -join '+')"
                myResponse = winexe_call(fqh, myDomain, myCommand)
                replace_property(
                    mySystem=mySystem[0],
                    key="CPU_CORES",
                    value=myResponse)
                # --Clear and register specific OSError
                # (Get-WmiObject -class Win32_OperatingSystem).Caption
                myCommand = "(Get-WmiObject -class Win32_OperatingSystem).Caption"
                myResponse = winexe_call(fqh, myDomain, myCommand)
                replace_property(
                    mySystem=mySystem[0],
                    key="OS",
                    value=myResponse)
                # --Clear and register all Networks adapters by MAC address

                # --Clear and register MACs

                myCommand = "Get-WmiObject win32_networkadapterconfiguration" \
                    " | select macaddress"
                myResponse = winexe_call(fqh, myDomain, myCommand)
                if " " in myResponse:
                    myResponse = myResponse.split()
                    myReturn = myResponse[1].strip().split("\n")
                else:
                    myReturn = ["RCP ERROR"]

                mySystem.update(owned_macs=",".join(myReturn))
                # removes all instances of a key for a system so be careful
                remove_property_key_exact(mySystem=mySystem[0], key="MAC")
                for myLine in myReturn:
                    # myLine=myLine.split()
                    add_property(mySystem=mySystem[0], key="MAC", value=myLine)
                    # --Clear and register all available networks
                    # ---clear and register IP
                    # ---Clear and register subnet
                    # ---clear and register adapter name if available

                # --Clear and register all WWNs
                # get-disk | select UniqueId, FriendlyName, Size | ft
                # -HideTableHeaders

                # myCommand = "get-disk | select UniqueId, Size, FriendlyName
                # | ft -HideTableHeaders | Out-String -Width 500"
                # myResponse = winexe_call(fqh, myDomain, myCommand)
                # myReturn = myResponse.strip().split("\n")
                # remove_property_key_exact(mySystem=mySystem[0], key = "WWN")
                # #removes all instances of a key for a system so be careful
                # for myLine in myReturn:
                #    myLine=myLine.split()
                #    add_property(mySystem = mySystem[0],
                #    key = "WWN", value = myLine[0])
                #    myName =  " ".join(myLine[2:-1])
                #    update_create_drive("HD " + myName.strip(),
                #    myLine[0], myLine[1])
                    # ---Check for existing HD entries for each
                    # WWN and auto add HDs if not found
                    # ---Register Size
                    # ---Register Type
                return "System " + \
                    str(mySystem[0].servicetag) + " updated through windows."

            if check_port(fqh, 22):
                # --Clear and register hostname
                replace_property(
                    mySystem=mySystem[0],
                    key="HOST",
                    value=hostname)

                # --store ip
                myIP = str(socket.gethostbyname(fqh))
                replace_property(mySystem=mySystem[0], key="IP", value=myIP)
                # --determine and store network
                myDomain = ""
                if myIP.startswith('172.'):
                    myDomain = "atc2"
                elif myIP.startswith('10.'):
                    myDomain = "www"
                replace_property(
                    mySystem=mySystem[0],
                    key="NETWORK",
                    value=myDomain)
                # --Clear and register operating system family as Linux
                replace_property(
                    mySystem=mySystem[0],
                    key="OS_FAMILY",
                    value="LINUX")
                # -if open Attempt ssh
                password = "cleverpassword"
                username = "root"
                try:
                    client = paramiko.SSHClient()
                    client.load_system_host_keys()
                    client.set_missing_host_key_policy(
                        paramiko.AutoAddPolicy())

                    client.connect(
                        fqh, port=22, username=username, password=password)
                    # --Clear and register bios version
                    stdin, stdout, stderr = client.exec_command(
                        "dmidecode -s bios-vendor && dmidecode "
                        "-s bios-version")
                    myReturn = stdout.read().replace("\n", " ").strip()
                    replace_property(
                        mySystem=mySystem[0], key="BIOS", value=myReturn)
                    # --Clear and register Processor Architecture
                    stdin, stdout, stderr = client.exec_command("uname -p")
                    myReturn = stdout.read().replace("\n", " ").strip()
                    replace_property(
                        mySystem=mySystem[0],
                        key="CPU_ARCHITECTURE",
                        value=myReturn)

                    # --Clear and register number of CPUs
                    stdin, stdout, stderr = client.exec_command(
                        "cat /proc/cpuinfo |grep 'physical id' "
                        "|sort -n |uniq |wc -l")
                    myReturn = stdout.read().replace("\n", " ").strip()
                    replace_property(
                        mySystem=mySystem[0],
                        key="CPU_SOCKETS",
                        value=myReturn)

                    # --Clear and register number of COREs
                    stdin, stdout, stderr = client.exec_command("nproc")
                    myReturn = stdout.read().replace("\n", " ").strip()
                    replace_property(
                        mySystem=mySystem[0],
                        key="CPU_CORES",
                        value=myReturn)

                    # --Clear and register kernel
                    stdin, stdout, stderr = client.exec_command("uname -r")
                    myReturn = stdout.read().replace("\n", " ").strip()
                    replace_property(
                        mySystem=mySystem[0],
                        key="KERNEL",
                        value=myReturn)

                    # --Clear and register MACs
                    stdin, stdout, stderr = client.exec_command(
                        "arp -n | grep ether")
                    myReturn = stdout.read().strip().split("\n")
                    # removes all instances of a key for a system so be careful
                    remove_property_key_exact(mySystem=mySystem[0], key="MAC")
                    for myLine in myReturn:
                        myLine = myLine.split()
                        add_property(
                            mySystem=mySystem[0], key="MAC", value=myLine[2])

                    replace_property(
                        mySystem=mySystem[0], key="SSH", value="AVAILABLE")
                except:
                    replace_property(
                        mySystem=mySystem[0],
                        key="SSH",
                        value="MISCONFIGURED")
                finally:
                    client.close()

                # --Clear and register all WWNs
                # ---Check for existing HD entries for each
                # WWN and auto add HDs if not found
                # ---Register Size
                # ---Register Type
                # --Clear and register all Networks adapters by MAC address

                # --Clear and register all available networks
                # ---clear and register IP
                # ---Clear and register subnet
                # ---clear and register adapter name if available
                # sut.save()
                return "System " + \
                    str(mySystem[0].servicetag) + " updated through linux"
            else:
                # sut.operating_system = "UNKNOWN"
                return "Exit with errors: system is neither linux nor windows"
        else:
            return "HOST " + fqh + " unreachable"

    return "System update failed unable to add through racadm"
